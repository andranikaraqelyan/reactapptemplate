/*API*/
class CRUDFetchAPI {

    /*requests*/
    static get(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.GET,
            is_id_after_path: true,
            is_send_body: false
        });
    }

    static getAll(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.GET,
            is_id_after_path: false,
            is_send_body: false
        });
    }

    static add(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.ADD,
            is_id_after_path: false,
            is_send_body: true
        });
    }

    static update(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.UPDATE,
            is_id_after_path: true,
            is_send_body: true
        });
    }

    static patch(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.PATCH,
            is_id_after_path: true,
            is_send_body: true
        });
    }

    static delete(data_type_code,data){
        Controls.request({
            data_type_code,
            data,
            method: Data.methods.DELETE,
            is_id_after_path: true,
            is_send_body: false
        });
    }

    /*data controls*/
    static setDataTypes(data_types){

        if(!Array.isArray(data_types))
            throw new TypeError('Data types must be array');

        data_types.forEach(data_type => {

            if(typeof data_type !== 'object')
                throw new TypeError('All data types items must be objects');

            if(!data_type.hasOwnProperty('code'))
                throw new TypeError('All data types items must contains option by name "code" ');

            if(typeof data_type.code !== 'string')
                throw new TypeError('All data types item.code must be string.');

            if(!data_type.hasOwnProperty('get'))
                throw new TypeError('All data types items must contains option by name "code" ');

            if(typeof data_type.get !== 'function')
                throw new TypeError('All data types item.get must be function.');

        });

        data_types.forEach(data_type => {
            if(data_types.filter( item => item.code === data_type.code ).length !== 1)
                throw new Error('Data types must have unique code');
        });

        Data.data_types = Object.assign([],data_types);

    }

    static setDefaultID(id_key){

        if(typeof id_key !== 'string')
            throw new Error('Id key must be string');

        Data.id_key = id_key;

    }

    static setAuthorizationToken(token_string){

        if(typeof token_string !== 'string')
            throw new Error('Authorization token must be string');

        Data.authorization_token = token_string;

    }

    static setAuthorizationType(authorization_type){

        if(!Utils.REQUIRED_AUTHORIZATION_TYPES.some( type => authorization_type === type))
            throw new Error('Authorization type can be one of this ' + Utils.REQUIRED_AUTHORIZATION_TYPES.join(','));

        Data.authorization_type = authorization_type;

    }

    static setAppendData(append_data){
        if(typeof append_data !== 'object')
            throw new Error(`Append data must be object`);

        Data.append_data = Object.assign({},append_data);
    }

    static setMethods(methods_object){

        if(typeof methods_object !== 'object')
            throw new Error('Methods object must be object');

        Object.keys(DEFAULT.METHODS).forEach( key =>{

            if(!Utils.REQUIRED_METHODS.some( method_code => method_code === methods_object[key] ))
                throw new Error('Request method can be one of this: ' + Utils.REQUIRED_METHODS.join(','));

            Data.methods[key] = methods_object[key];

        });
    }

    static setAutoRemoveIDKey(auto_remove){

        Data.auto_remove_id_key = Boolean(auto_remove);

    }

    static setServerHost(server_host){

        if(typeof server_host !== 'string')
            throw new Error('Server host must be string');

        Data.server_host = server_host;

    }

    static setServerPath(server_path){

        if(typeof server_path !== 'string')
            throw new Error('Server path must be string');

        Data.server_path = server_path;

    }

    static setDevMode(dev_mode){

        Data.dev_mode = Boolean(dev_mode);

    }

    static getOptions(){

        return Object.assign({},Data);

    }

}

class Controls {

    static request({data_type_code, data, method, is_id_after_path, is_send_body}){

        return new Promise((resolve,reject) => {

            let log = [];

            /*validation*/
            if(!Data.data_types.some( data_type => data_type.code === data_type_code ))
                throw new Error('Data type by code' + data_type_code + ' not exist');

            if(typeof data !== 'object')
                data = {};

            let cb_result;
            try{
                cb_result = Data.data_types.find( data_type => data_type.code ===data_type_code ).get(data);
            }
            catch(err){
                return reject('Your callback throws error, Please fix it.\nError: ' + err);
            }

            if(!cb_result.hasOwnProperty('path') || typeof cb_result.path !== 'string')
                return reject('Your callback must return object in this template  {path: (string)} ');

            /* path generation */
            let path = Data.server_host + Data.server_path;
            path += cb_result.path;
            if(is_id_after_path) path += '/' + data[Data.id_key];

            /* init headers */
            const options = {};
            options.headers = {};
            if(Data.authorization_token !== null)
                options.headers['Authorization'] = Data.authorization_type + ' ' + Data.authorization_token;
            options.headers['Content-Type'] = 'application/json';
            options.method = method;

            /* body*/
            if(is_send_body){
                options.body = Object.assign({},data);
                if(Data.auto_remove_id_key)
                    delete options.body[Data.id_key];
                for(let key in Data.append_data){
                    options.body[key] = Data.append_data[key];
                }
                options.body = JSON.stringify(options.body);
            }

            if(Data.dev_mode){
                log.push(
                    LOG_DELIMITERS.START,
                    LOG_DELIMITERS.REQUEST,
                    '| Method: ', method, '\n',
                    '| Path: ', path, '\n'
                );
                if(is_send_body){
                    log.push(
                        '| Body: ', options.body, '\n'
                    );
                }
                log.push(
                    '| Headers: ', options.headers, '\n',
                    LOG_DELIMITERS.RESPONSE
                )
            }

            let promise_result = null;

            fetch(path,options).then(
                response => {
                    promise_result =  Controls.postHandle(response,resolve,reject,log);
                },
                err => {
                    if(Data.dev_mode){
                        log.push(
                            '| Fetch error', '\n'
                        );
                    }
                    promise_result = reject(err);
                }
            ).then(
                () => {
                    if(Data.dev_mode){
                        log.push(
                            LOG_DELIMITERS.END
                        );
                        console.log(...log);
                    }
                    return promise_result;
                }
            );



        });

    }

    static postHandle(response,resolve,reject,log){

        const result_data = {};
        let result_promise = null;
        result_data.status = response.status;

        response.json().then(
            data => {

                result_data.data = data;

                switch(response.status){
                    case 200:
                        result_promise = resolve(result_data);
                        break;
                    default:
                        result_promise = reject(result_data);
                }

                if(Data.dev_mode){
                    log.push(
                        '| Data: ', result_data.data, '\n',
                        '| Status: ', result_data.status, '\n'
                    );
                }

            },
            (error) => {
                result_data.error_message = 'Server send no-json data.';
                if(Data.dev_mode) {
                    log.push(
                        '| Error: ', result_data.error_message, '\n'
                    );
                }
                result_promise = reject(result_data);
            }
        ).then(
            () => {
                return result_promise;
            }
        );

    }

}

const DEFAULT = {
    METHODS: {
        GET: 'get',
        ADD: 'post',
        UPDATE: 'put',
        PATCH: 'path',
        DELETE: 'delete'
    }
};

const Data = {

    authorization_token: null,
    authorization_type: 'Bearer',
    id_key: 'id',
    append_data: {},
    methods: Object.assign({},DEFAULT.METHODS),
    data_types: [],
    auto_remove_id_key: false,
    dev_mode: false,
    server_host: null,
    server_path: ''

};

class Utils {}
Utils.REQUIRED_METHODS = [
    'get',
    'post',
    'put',
    'delete',
    'patch',
    'options',
    'head',
    'connect',
    'trace'
];

Utils.REQUIRED_AUTHORIZATION_TYPES = [
    'Basic',
    'Bearer',
    'Digest',
    'HOBA',
    'Mutual'
];

const LOG_DELIMITERS = {
    START:    '\n /---------------------------------------\n',
    END:      '\\---------------------------------------\n\n',
    REQUEST:  '|--------- Request ---------------------\n |---------------------------------------\n',
    RESPONSE: '|---------------------------------------\n |--------- Response --------------------\n |---------------------------------------\n',
};

export {CRUDFetchAPI};