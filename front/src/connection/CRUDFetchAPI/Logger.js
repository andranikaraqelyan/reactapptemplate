class Logger {

    constructor(){
        this.log = [];
    }

    /*methods*/
    start(){}

    end(){}

    request(){}

    response(){}

    method(method){}

    path(path){}

    headers(headers){}

    receivedData(data){}

    status(status){}

    error(error){}

}

const LOG_DELIMITERS = {
    START:    '\n /---------------------------------------\n',
    END:      '\\---------------------------------------\n\n',
    REQUEST:  '|--------- Request ---------------------\n |---------------------------------------\n',
    RESPONSE: '|---------------------------------------\n |--------- Response --------------------\n |---------------------------------------\n',
};

export {Logger};