import React from 'react';

const B = (props) => {

    const params = [];

    for(let key in props.routing.params){

        params.push(
            <span key={key}><br/>{key} : {props.routing.params[key]}</span>
        );

    }

    return (
        <div>
            <span>B</span>
            {params}
        </div>
    );
};

export {B};