import React from 'react';

const A = (props) => {

    const params = [];

    for(let key in props.routing.params){

        params.push(
            <span key={key}><br/>{key} : {props.routing.params[key]}</span>
        );

    }

    return (
        <div>
            <span>A</span>
            {params}
        </div>
    );
};

export {A};