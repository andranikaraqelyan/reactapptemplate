import React from 'react';
import {CRUDFetchAPI} from "connection/CRUDFetchAPI";

class App extends React.Component {


    componentDidMount(){
        /*Crud fetch test*/
        CRUDFetchAPI.setDevMode(true);
        console.log(CRUDFetchAPI.getOptions());
        CRUDFetchAPI.setServerHost('http://localhost:3001');
        CRUDFetchAPI.setDataTypes([
            {
                code: 'users',
                get: data => ({path: '/users'})
            }
        ]);
        CRUDFetchAPI.getAll('users');
    }

    render(){
        return (
            <div id="app">
                App
            </div>
        );
    }
}

export default App;