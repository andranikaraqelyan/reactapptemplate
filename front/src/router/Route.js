import {Router} from './Router';
import React from 'react';

class Route extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            path: ''
        };

        this.initRegExp();

        Router.subscribe(path=>{
            this.setState({
                path
            });
        });
    }

    initRegExp(){
        /*  @indev  */
        let path = this.props.path;
        this.params = [];
        if(!/\/:\w+/.test(path)){
            this.pathRegExp = new RegExp(path);
            return;
        }
        let regExpStr = '';
        while(path){
            const match = path.match(/\/:\w+/);
            const index = match.index;
            const paramStr = match[0];
            path = path.slice(index+paramStr.length);
            this.params.push(paramStr.slice(2));
            regExpStr += match.input.slice(0,index);
            regExpStr += '(/\\w+)';
        }
        regExpStr = `^` + regExpStr + `$`;
        this.pathRegExp = new RegExp(regExpStr);
    }

    render(){

        const match = this.state.path.match(this.pathRegExp);
        const childProps = {
            routing: {
                params: {}
            }
        };
        if(match && this.params){
            this.params.forEach((v,index)=>{
                childProps.routing.params[v] = match[index+1].slice(1);
            });
        }
        return this.props.component && match?React.createElement(this.props.component,childProps):null;

    }
}

export {Route};