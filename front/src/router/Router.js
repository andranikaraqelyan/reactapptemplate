import {Subject} from 'rxjs';

class Router {

    static redirect(new_path){

        if(typeof new_path !== 'string')
            observer.next({error_message: 'Path must be string'});
        else{
            window.history.pushState({},'',new_path);
            observer.next(new_path);
        }


    }

    static subscribe(cb1,cb2,cb3){

        observer.subscribe(cb1,cb2,cb3);

    }

    static initFromLocation(){
        // hnaravora initi vaxt arvi, bayc es pahin kuzenayi ruchnoy, vor asenq token ete chka, petq chi routing anel ej@ bacelu vaxt ev ayln ev ayln ....
        observer.next(window.location.pathname);

    }

}

const observer = new Subject();

export {Router};